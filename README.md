IndexExtractor readme file
========================================

Check website every hour for desired information.
Extract information and email it.

===========================================

SETUP:

SET UP VIRTUALENV:
- install virtualenv
- pip install requirements.txt


INSTALL DEPENDENCIES/ LIBRARIES:

System wide (sudo apt-get install):

- xvfb (to hide browser)
- xserver-xephyr (to hide browser)
- poppler-utils (for pdftoxml to work)


IN THE CODE:
- issues with libraries: comment out decoding in scraperwiki:
in utils.py in scraperwiki module uncomment .decode('utf-8') in return statement


SET UP SERVER:
DigitalOcean or similar with Ubuntu 14.04 LTS

SET UP FILEZILLA:
Install on computer
Use IP of the server to connect.
Transfer files onto Ubunu on the server.


SET UP CRONTAB:
type in terminal:
sudo crontab -e

Add line at the bottom:
0 * * * * /pathtopythoninvirtualenvironment/python /pathtoprojectfile/extract_pdf.py >> /tmp/botlog.log 2>&1
This will run the program every hour.

- On the server run crontab scheduled job by typin into terminal sudo crontab -e and then
add this line into the file:
0 * * * * /home/matjaz/Envs/venv_index_exractor/bin/python3 /home/matjaz/PycharmProjects/IndexExtractor/extract_pdf.py >> /tmp/botlog.log 2>&1
PS: If you want to run extract_pdf.py file from terminal put sudo at the begininng. But you don't need sudo in crontab command!!
Not sure why that is. Something about specifying DISPLAY environmental variable for pyvirtualdisplay
(see for more: https://stackoverflow.com/questions/27270337/how-to-fix-webdriverexception-the-browser-appears-to-have-exited-before-we-coul)

=========================

CODE FLOW:

1. Website is accessed.
2. Selenium renders the page source and ensures that any javascript is rendered into HTML (this is to avoid any JS links)
3. get_urls() function iterates through all links on the page and stores them in a list
4. scrape(url) method takes individual url and scrapes indices from the PDF.
5. scrape() method scrapes PDF only for info paragraph, investibility index and date
Indice tile and date of release that go into email subject are scraped from the original page where the links are. This is because they are not easy to scrape from PDF. Other content can come before the title which inteferes with scraping.
6. run() function iterates through the list of urls and scrapes each of them.
7. Scraped content is stored into a text file.
8. Each hour when the program runs it checks against the stored content in txt files. If content is already there then
the program doesn't pick up those indices. It only picks up indices that are new.
9. FInaly all indices in that hour are emailed to the user.

Code is structured with inheritance.
Extractor() class is the base class. ExtractorWorld, ExtractorUK_Ftse_100 and ExtractorUK_Ftse_250 are subclasses
that inherit from the base class. Subclasses inherit all methods except scrape(url) and run(). Instead these methods are overriden because they differ for each subclass (for example ExtractorWorld scrapes in scrape method FTSE All WOrld indices while UK scrape UK 100 and 250). run() method also differs because txt files are named differently.

It is easy to add another page to scrape. But the page needs to be a derivative of or look similar to FTSE/GEISAC page because
the whole scraping mechanism is tailored for that website.
For a completely different website with different structure of links etc, new code will have to be written.


==========================

OTHER:
- cron job - files that read in the code (open, read, write) need to have absolute paths !!
Otherwise crontab won't find the files.
(correct example: with open('/home/matjaz/py_projects/IndexExtractor/somefile.txt))
FOR DEBUGGING CRONTAB >> /tmp/botlog.log 2>&1 is added at the end of cron command to see any errors.
Errors can be seen by typing vim tmp/botlog.log in server terminal
Note that that text file won't be visible in tmp folder, but you can still open it with vim

- On the server run crontab scheduled job by typin into terminal sudo crontab -e and then
add this line into the file - adjust paths to your own:
0 * * * * /home/matjaz/Envs/venv_index_exractor/bin/python3 /home/matjaz/PycharmProjects/IndexExtractor/extract_pdf.py >> /tmp/botlog.log 2>&1
PS: If you want to run extract_pdf.py file from terminal put sudo at the begininng. But you don't need sudo in crontab command!!
Not sure why that is. Something about specifying DISPLAY environmental variable for pyvirtualdisplay
(see for more: https://stackoverflow.com/questions/27270337/how-to-fix-webdriverexception-the-browser-appears-to-have-exited-before-we-coul)

- Potential problem: slow internet connection->slow loading of the page? apply wait time?

- if sending emails from gmail, note that gmail is limited to 500 emails/recipients per day
and also activated security issues when ran from server IP. To avoid that don't use gmail for sending :)

- email receipients are in recipients.txt file. Add new recepient in a new line. Not in the same row. No commas.

- email sender credentials are also each in its own row. No commas.

- Structure:

SERVER  ----->  where original code runs every hour, Includes virtualenv, project folder, cron job
VIRTUAL ENV	 ----->  isolated python environment where installed libraries are placed
PROJECT FOLDER	----->  Python files, txt files (credentials, recepients, data storing)
CRON JOB  ----->  any errors can bee seen by typing vim tmp/botlog.log in server terminal
FILEZILLA -> use it to upload code to server - or can pull from itlab
YOUR LOCAL MACHINE
- has the same environment, just that it's local and for development.
- Upload changes via Filezilla or Gitlab


- Libraries to import in the code:

import re
import scraperwiki
import requests
from selenium import webdriver
from selenium.common.exceptions import *
from bs4 import BeautifulSoup
from datetime import datetime, date, time
import smtplib
from email.mime.text import MIMEText
from pyvirtualdisplay import Display


