__author__ = 'matjaz'

"""
    Check website every hour for desired information.
    Extract information and email it.

    Dependencies:
    System wide (sudo apt-get install):
    xvfb
    xserver-xephyr
    poppler-utils (for pdftoxml to work)
    Venv: see requirements.txt

    CRON
    - VERY IMPORTANT: WHEN USING CRONTAB MAKE SURE YOU USE ABSOLUTE PATHS
    IN FILES THAT ARE READ OR WRITTEN  !!!! Otherwise crontab won't find the files.
    - FOR DEBUGGING ADD >> /tmp/botlog.log 2>&1 at the end of crontab line to see any errors.
    Note that that text file won't be visible in tmp folder, but you can still open it with vim

    """

import re
import scraperwiki
import requests
from selenium import webdriver
from selenium.common.exceptions import *
from bs4 import BeautifulSoup
from datetime import datetime, date, time
import smtplib
from email.mime.text import MIMEText
from pyvirtualdisplay import Display




# BASE CLASS
class Extractor:
    def __init__(self, url, file_path):
        self.url = url
        self.file_path = file_path
        self.result = []
        self.source = ''


    def page_source(self):
        """
         Gets page source, renders all JS if needed (by Selenium)
        :return: string
        """
        # install Xvfb and then pyvirtualdisplay
        # See solution: https://stackoverflow.com/questions/13039530/unable-to-call-firefox-from-selenium-in-python-on-aws-machine
        # install (apt-get) xvfb if don't want to have firefox to be visible Display(visible=0,...)
        # install xserver-xephyr if you want to display firefox (Display(visible=1,...)
        # of both if you want to switch between the two

        display = Display(visible=0, size=(1024, 768))  # hides browser window from showing
        display.start()

        driver = webdriver.Firefox()

        # get URL
        try:
            driver.get(self.url)
        except NoSuchElementException as exc:
            print("URL not found: ", exc)

        # get page source (from selenium to render Javascript. However I saw Js didn't render!!! - to check)
        self.source = driver.page_source
        driver.close()
        display.stop()

        return self.source

    def get_urls(self):
        """
        Gets list of URLs from ftse website (from links).
        :return: list of URLs
        """
        # extract main body of URL and id number from page source
        p = re.compile(r'/products/index-notices/home/getnotice/\?id=\d+')
        urls = p.findall(self.page_source())

        # prepend 'http://www.ftse.com'
        urls = ['http://www.ftse.com' + part for part in urls]

        return urls

    def scrape(self, url):
        """
        This method is overriden by subclass methods.
        Extracts data from the pdf file.
        First pdf is converted to xml.
        Then Beautifulsoup is used to get the text from XML.
        Then regex is used to extract data from the text.
        :param url: url as list of urls
        :return: 6-element list of required data
        """
        pass

    def credentials(self):
        """
        Protect sender's email and password by pulling them from config file
        (Rather than hard-coding them)
        :return:
        """
        with open('/home/matjaz/PycharmProjects/IndexExtractor_sensitive_info/credentials.txt', 'r') as f:
            lines = f.readlines()
            username = lines[0].strip()
            password = lines[1].strip()
            return (username, password)

    def recipients(self):
        """
        Get list of recipients to send email to.
        :return: list
        """
        with open('/home/matjaz/PycharmProjects/IndexExtractor_sensitive_info/recipients.txt', 'r') as f:
            lines = f.readlines()
            recipients = [line.strip() for line in lines]
            return recipients

    def email(self, file):
        print('\nSending email...')

        # skips first element in line
        print('Email content-before cleanup: ', file)
        title = file[0]
        release_date = file[1]

        msg_text = ''
        for line in file[2:]:
            if not line == None:
                msg_text += (str(line) + '\n\n')

        # Create a text/plain message
        msg = MIMEText(msg_text)

        # unpack sender's email and password from credentials() function
        email_from, password = self.credentials()

        # website = 'FTSE'
        # date_time = str(datetime.utcnow())[:19]
        msg['Subject'] = '{}, {}'.format(title, release_date)
        msg['From'] = email_from
        msg['To'] = ', '.join(self.recipients())

        # Send the message via our own SMTP server.
        s = smtplib.SMTP(host='smtp.kolabnow.com', port=587)
        s.ehlo()
        s.starttls()
        s.login(email_from, password)
        s.send_message(msg)
        s.quit()
        print('Email sent.')


    def run(self):
        print('-----   ' + str(datetime.now()))

        for ind, url in enumerate(self.get_urls(), 1):
            print('==============')
            print(ind)
            sp = self.scrape(url)

            if not sp == None:
                print('Data saved.')
                self.result.append(sp)  # list of lists  [ [a,b,c,d], [a,b,c,d], [a,b,c,d],... ]

        # reverse order so latest indice will be emaled first, most recent last
        # mimic reading links bottom up on FTSE website, indices at the bottom are emailed first, at the top last
        self.result.reverse()

        with open(self.file_path, 'r') as r:
            previous = r.read()

            # email only new indexes (results)
            # ignore those that are the same from previous scrape - check against text file
            latest = [i for i in self.result if not str(i) in previous]

            # send separate email for each indice within the last hour
            if not len(latest) == 0:
                for indice in latest:
                    self.email(indice)
            else:
                print('\nNo new indices. No email sent.')

        # store current result in a file
        # writing to file only for storing purposes to compare against new result.
        # not intended to be read by user
        with open(self.file_path, 'w') as w:
            w.write(str(self.result))
            print('\nResult written to file.') 



# SUBCLASS 1
class ExtractorWorld(Extractor):
    """
        Subclass to extract 'FTSE All-World Index' indices from GEISAC website.
    """
    def scrape(self, url):
        """
        Extracts data from the pdf file.
        First pdf is converted to xml.
        Then Beautifulsoup is used to get the text from XML.
        Then regex is used to extract data from the text.
        :param url: url as list of urls
        :return: 6-element list of required data
        """
        # SCRAPERWIKI   ===================================
        # get XML from PDF
        pdfdata = requests.get(url).content
        xmldata = scraperwiki.pdftoxml(pdfdata)  # in utils.py I uncommented .decode('utf-8') in return statement

        # BEAUTIFULSOUP   ================================================
        # get text from XML
        soup = BeautifulSoup(xmldata, 'html.parser')
        # print(soup)

        indice_type = 'FTSE All-World Index'
        invest_weigh = 'investability weighting'

        data_list = []

        if indice_type in soup.text:
            if invest_weigh in soup.text:
                t = soup.text.strip()
                # print(t)

                # EXTRACT DATA   ==============================

                # extract title from HTML
                # get title from the page source rather than from PDF
                # this is because it's difficult to extract title from PDF because
                # sometimes there additional text can appear before the title  that intefers with extraction
                # using beautiful soup syntax to find tag that contains the title
                soup_html = BeautifulSoup(self.source, 'html.parser')
                new_url = url.split('.com')[1]

                anchor_tag = soup_html.find(href=new_url)
                title_long = anchor_tag.text
                title = title_long.split(':')[0]
                data_list.append(title)

                # extract date from HTML
                a = soup_html.find('a',
                                   href=new_url)  # get specific link that contains indice url that we're retrieving
                par = a.parent.parent  # go two levels up to get to parent of the tag where the date is
                date = par.find(
                    'td').text.strip()  # date is the first td tag inside tr tag. find() method retrieves the first td tag
                data_list.append(date)

                # extract info from PDF
                p = re.compile(r'20\d+\s+\n(.*?:)', re.DOTALL)
                info = p.search(t)
                info = info.group(1).strip().replace('\n', '')  # group and clean up
                data_list.append(info)

                # extract investibility data
                p = re.compile(r'(FTSE\s+All-World\s+Index)(.*?)(\d+\s+\w+\s+201\d)', re.DOTALL)
                inv_data = p.findall(t)
                # remove newline characters
                inv_data_list = [i.strip().replace('\n', '') if '\n' in i else i.strip() for element in inv_data for i
                                 in element]
                # extend data_list that contains info paragraph with the investability data
                data_list.extend(inv_data_list)

                if not len(data_list) == 0:
                    return data_list  # list ['a', 'b', 'c', 'd', 'e', 'f']
                else:
                    print('No data.')
                    return None
            else:
                print('Investability weighting string NOT found.')
        else:
            print('FTSE All-World Index string NOT found.')


# SUBCLASS 2
class ExtractorUK_Ftse_100(Extractor):
    """
        Subclass to extract 'FTSE 100 Index' indices from UK website.
    """

    def scrape(self, url):
        """
        Extracts data from the pdf file.
        First pdf is converted to xml.
        Then Beautifulsoup is used to get the text from XML.
        Then regex is used to extract data from the text.
        :param url: url as list of urls
        :return: 6-element list of required data
        """
        # SCRAPERWIKI   ===================================
        # get XML from PDF
        pdfdata = requests.get(url).content
        xmldata = scraperwiki.pdftoxml(pdfdata)  # in utils.py I uncommented .decode('utf-8') in return statement

        # BEAUTIFULSOUP   ================================================
        # get text from XML
        soup = BeautifulSoup(xmldata, 'html.parser')
        # print(soup)

        indice_type = 'FTSE 100 Index'
        invest_weigh = 'investability weighting'

        data_list = []

        if indice_type in soup.text:
            if invest_weigh in soup.text:
                t = soup.text.strip()
                # print(t)

                # EXTRACT DATA   ==============================

                # extract title from HTML
                # get title from the page source rather than from PDF
                # this is because it's difficult to extract title from PDF because
                # sometimes there additional text can appear before the title  that intefers with extraction
                # using beautiful soup syntax to find tag that contains the title
                soup_html = BeautifulSoup(self.source, 'html.parser')
                new_url = url.split('.com')[1]

                anchor_tag = soup_html.find(href=new_url)
                title_long = anchor_tag.text
                title = title_long.split(':')[0]
                data_list.append(title)

                # extract date from HTML
                a = soup_html.find('a',
                                   href=new_url)  # get specific link that contains indice url that we're retrieving
                par = a.parent.parent  # go two levels up to get to parent of the tag where the date is
                date = par.find(
                    'td').text.strip()  # date is the first td tag inside tr tag. find() method retrieves the first td tag
                data_list.append(date)

                # extract info from PDF
                p = re.compile(r'20\d+\s+\n(.*?:)', re.DOTALL)
                info = p.search(t)
                info = info.group(1).strip().replace('\n', '')  # group and clean up
                data_list.append(info)

                # extract investibility data
                p = re.compile(r'(FTSE\s+100\s+Index)(.*?)(\d+\s+\w+\s+201\d)', re.DOTALL)
                inv_data = p.findall(t)
                # remove newline characters
                inv_data_list = [i.strip().replace('\n', '') if '\n' in i else i.strip() for element in inv_data for
                                 i in element]
                # extend data_list that contains info paragraph with the investability data
                data_list.extend(inv_data_list)

                if not len(data_list) == 0:
                    return data_list  # list ['a', 'b', 'c', 'd', 'e', 'f']
                else:
                    print('No data.')
                    return None
            else:
                print('Investability weighting string NOT found.')
        else:
            print('FTSE 100 Index string NOT found.')


class ExtractorUK_Ftse_250(Extractor):
    """
        Subclass to extract 'FTSE 250 Index' indices from UK website.
    """

    def scrape(self, url):
        """
        Extracts data from the html and pdf file.
        First pdf is converted to xml.
        Then Beautifulsoup is used to get the text from XML.
        Then regex is used to extract data from the text.
        :param url: url as list of urls
        :return: 6-element list of required data
        """
        # SCRAPERWIKI   ===================================
        # get XML from PDF
        pdfdata = requests.get(url).content
        xmldata = scraperwiki.pdftoxml(pdfdata)  # in utils.py I uncommented .decode('utf-8') in return statement

        # BEAUTIFULSOUP   ================================================
        # get text from XML
        soup = BeautifulSoup(xmldata, 'html.parser')
        # print(soup)

        indice_type = 'FTSE 250 Index'
        invest_weigh = 'investability weighting'

        data_list = []

        if indice_type in soup.text:
            if invest_weigh in soup.text:
                t = soup.text.strip()
                # print(t)

                # EXTRACT DATA   ==============================

                # extract title from HTML
                # get title from the page source rather than from PDF
                # this is because it's difficult to extract title from PDF because
                # sometimes there additional text can appear before the title  that intefers with extraction
                # using beautiful soup syntax to find tag that contains the title
                soup_html = BeautifulSoup(self.source, 'html.parser')
                new_url = url.split('.com')[1]

                anchor_tag = soup_html.find(href=new_url)
                title_long = anchor_tag.text
                title = title_long.split(':')[0]
                data_list.append(title)

                # extract date from HTML
                a = soup_html.find('a',
                                   href=new_url)  # get specific link that contains indice url that we're retrieving
                par = a.parent.parent  # go two levels up to get to parent of the tag where the date is
                date = par.find(
                    'td').text.strip()  # date is the first td tag inside tr tag. find() method retrieves the first td tag
                data_list.append(date)

                # extract info from PDF
                p = re.compile(r'20\d+\s+\n(.*?:)', re.DOTALL)
                info = p.search(t)
                info = info.group(1).strip().replace('\n', '')  # group and clean up
                data_list.append(info)

                # extract investibility data
                p = re.compile(r'(FTSE\s+250\s+Index)(.*?)(\d+\s+\w+\s+201\d)', re.DOTALL)
                inv_data = p.findall(t)
                # remove newline characters
                inv_data_list = [i.strip().replace('\n', '') if '\n' in i else i.strip() for element in inv_data for i
                                 in element]
                # extend data_list that contains info paragraph with the investability data
                data_list.extend(inv_data_list)

                if not len(data_list) == 0:
                    return data_list  # list ['a', 'b', 'c', 'd', 'e', 'f']
                else:
                    print('No data.')
                    return None
            else:
                print('Investability weighting string NOT found.')
        else:
            print('FTSE 250 Index string NOT found.')



# ========   RUN EXTRACTORS   =======

print('\n-----   FTSE All-World Index')
world = ExtractorWorld('http://www.ftse.com/products/index-notices/home/getnotices/?id=GEISAC',
                       '/home/matjaz/PycharmProjects/IndexExtractor/result_world.txt')

world.run()

print('\n-----   UK FTSE 100 Index')
uk_ftse_100 = ExtractorUK_Ftse_100('http://www.ftse.com/products/index-notices/home/getnotices/?id=UK',
                                   '/home/matjaz/PycharmProjects/IndexExtractor/result_uk_100.txt')
uk_ftse_100.run()

print('\n-----   UK FTSE 250 Index')
uk_ftse_250 = ExtractorUK_Ftse_250('http://www.ftse.com/products/index-notices/home/getnotices/?id=UK',
                                   '/home/matjaz/PycharmProjects/IndexExtractor/result_uk_250.txt')
uk_ftse_250.run()



